﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SH.utilidades
{
    public class Constants
    {
        public readonly static string ARQUIVOPARTICIPANTES = "lista_pessoas";
        public readonly static int RENDACORTEMINIMO = 1045;
        public readonly static int RENDACORTEMAXIMO = 5225;
        public readonly static string GERAL = "geral";
        public readonly static string DEFICIENTE = "deificente físico";
        public readonly static string IDOSO = "idoso";
        public readonly static int IDADEIDOSO = 60;
        public readonly static int IDADEMINIMA = 15;
        public readonly static int NUMERODECAMPOSPARTICIPANTE = 6;
        public readonly static List<string> GRUPOSPARTICIPANTES = new() { GERAL, IDOSO, DEFICIENTE };
    }
}
