﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SH.utilidades
{
    public class ArquivosUtilidades
    {
        public static IConfiguration ConfigurationBuilder(/*bool isApi = false*/)
        {
            string repositorioAtual = Directory.GetCurrentDirectory();
            List<string> caminhos = repositorioAtual.Split("\\").ToList();

            if (caminhos[0] == repositorioAtual)
                caminhos = repositorioAtual.Split("/").ToList();

            while (true)
            {
                if (caminhos.Last() == "Sistema de Habitacao")
                    break;
                caminhos.RemoveAt(caminhos.Count - 1);

            }
            caminhos.Add("SH.api");
            var path = Path.Combine(caminhos.ToArray());


            return new ConfigurationBuilder()
          .SetBasePath(path)
          .AddJsonFile($"appsettings.json", optional: true, reloadOnChange: false)
          .Build();

        }

        public static StreamReader CarregarArquivo(string nome, string extensao = "csv") {
            string repositorioAtual = Directory.GetCurrentDirectory();
            List<string> caminhos = repositorioAtual.Split("\\").ToList();

            if (caminhos[0] == repositorioAtual)
                caminhos = repositorioAtual.Split("/").ToList();


            while (true)
            {
                if (caminhos.Last() == "Sistema de Habitacao")
                    break;
                caminhos.RemoveAt(caminhos.Count - 1);

            }
            caminhos.Add(ConfigurationBuilder()["DiretorioDados"]);
            var path = Path.Combine(caminhos.ToArray());
            return new StreamReader($@"{path}{nome}.{extensao}");
        } 

    }
}
