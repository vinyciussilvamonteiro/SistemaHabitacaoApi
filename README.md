# Sistema de Habitação - API



## Api utilizada para realização do sorteio de habitação.
Neste repositório se encontra apenas o **BACKEND** da solução.

### Tecnologias utilizadas:
 -  [.NET core 5] - Versão do framework Asp Net Core;
 -  [C#] - Linguagem orientada a objetos da Microsoft;
 - [xUnit] - Framework para realização de teste;

### Recursos:
 - Recuperar Lista de Aptos ao Sorteio por categoria( Idosos, Deficientes, Geral);
 - Realizar sorteio;


### Como utilizar:

Clone o repositorio a partir do seguinte endereço: 
```sh
https://gitlab.com/vinyciussilvamonteiro/SistemaHabitacaoApi
```
Acesse a pasta do projeto, pela interface do sistema ou pelo comando:
```sh
cd Sistema de Habitacao
```
Restaure os pacotes Nugets, ao selecionar a opção de restauração na interface ou pelo comando:
```sh
nuget restore Sistema de Habitacao.sln
```
**_Observação:_**  Geralmente o próprio Visual studio ja se encarrega disso.

**Inicie o projeto a partir do IIS Express.**


### Como Realizar Teste:
Para a realização dos testes unitários é necessário ter a extensão, **[Microsoft Visual Studio Test Platform]**.
Os Testes podem ser realizados a partir do **Menu superior > Test > Test Explorer** ou do comando **CTRL+R,A**.


### Front End

O projeto de font end se localiza em:
[https://gitlab.com/vinyciussilvamonteiro/SistemaDeHabitacaoFront]



   [.NET core 5]: <https://dotnet.microsoft.com/download/dotnet/5.0>
   [C#]: <https://docs.microsoft.com/pt-br/dotnet/csharp/>
   [xUnit]: <https://xunit.net/>
   [https://gitlab.com/vinyciussilvamonteiro/SistemaDeHabitacaoFront]: <https://gitlab.com/vinyciussilvamonteiro/SistemaDeHabitacaoFront>
   [Microsoft Visual Studio Test Platform]: <https://docs.microsoft.com/pt-br/visualstudio/test/improve-code-quality?view=vs-2019>
   
