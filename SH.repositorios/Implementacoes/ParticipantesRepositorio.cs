﻿using SH.modelos;
using SH.repositorios.Interfaces;
using SH.utilidades;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SH.repositorios.Implementacoes
{
    public class ParticipantesRepositorio: IParticipantesRepositorio
    {
        
        private readonly List<Participante> _participantes;
        public ParticipantesRepositorio()
        {
            StreamReader arquivo = ArquivosUtilidades.CarregarArquivo(Constants.ARQUIVOPARTICIPANTES);
            _participantes = ExtrairUsuarios(arquivo);

        }

        public List<Participante> RecuperarIdososAptos() =>
            _participantes
                .FindAll(
                    participante => participante.Idade > Constants.IDADEIDOSO && 
                    participante.Renda >= Constants.RENDACORTEMINIMO && 
                    participante.Renda <= Constants.RENDACORTEMAXIMO &&
                    participante.Cota.ToLower() == Constants.IDOSO &&
                    DadosUtilidades.ChecarValidadeCpf(participante.Cpf)
                );
        
        public List<Participante> RecuperarDeficientesAptos() => 
            _participantes
                .FindAll(
                    participante => participante.Idade > Constants.IDADEMINIMA && 
                    participante.Renda >= Constants.RENDACORTEMINIMO &&
                    participante.Renda <= Constants.RENDACORTEMAXIMO && 
                    DadosUtilidades.ChecarValidadeCpf(participante.Cpf) &&
                    participante.Cota.ToLower() == Constants.DEFICIENTE &&
                    !string.IsNullOrEmpty(participante.Cid)
                );

        public List<Participante> RecuperarGeralAptos() =>
            _participantes
                .FindAll(
                    participante => participante.Idade > Constants.IDADEMINIMA &&
                    participante.Renda >= Constants.RENDACORTEMINIMO &&
                    participante.Renda <= Constants.RENDACORTEMAXIMO &&
                    participante.Cota.ToLower() == Constants.GERAL &&
                    DadosUtilidades.ChecarValidadeCpf(participante.Cpf)
                );

        public List<CategoriaComParticipantes> RecuperarTodosAptos()
        {
            List<Participante> idosos = RecuperarIdososAptos();
            List<Participante> deficientes = RecuperarDeficientesAptos();
            List<Participante> geral = RecuperarGeralAptos();

            List<CategoriaComParticipantes> 
                todosOsParticipanteAptos = new List<Participante>()
                    .Concat(idosos)
                    .Concat(deficientes)
                    .Concat(geral)
                    .GroupBy(participante => participante.Cota)
                    .Select(
                        grupo => 
                            new CategoriaComParticipantes { 
                                Nome = grupo.Key,
                                Participantes = grupo.ToList() 
                            }
                    )
                    .ToList();

            return todosOsParticipanteAptos;
        }



        private static List<Participante> ExtrairUsuarios(StreamReader arquivo)
        {
            List<Participante> participantes = new();
            string linha;
            while ((linha = arquivo.ReadLine()) != null)
            {


                List<string> dados = new(linha.Split(","));

                if (dados.Count < 6)
                    dados = EqualizarDados(dados);
                

                if (!DadosUtilidades.ChecarNome(dados[0]))
                    continue;

                if (!DadosUtilidades.ChecarComprimentoCpf(dados[1]))
                    continue;

                if (!DadosUtilidades.ChecarValidadeDataNascimento(dados[2]))
                    continue;

                if (!DadosUtilidades.ChecarValorNumerico(dados[3]))
                    continue;

                if (!DadosUtilidades.ChecarGruposValidos(dados[4], Constants.GRUPOSPARTICIPANTES))
                    continue;


                Participante ParticipanteSemErro = new(dados[0], dados[1], dados[2],dados[3],dados[4],dados[5]);

                participantes.Add(ParticipanteSemErro);

            }

            return participantes;
        }

        private static List<string> EqualizarDados(List<string> dados)
        {
            int valorAdicionar = Constants.NUMERODECAMPOSPARTICIPANTE - dados.Count;

            for (int i = 0; i < valorAdicionar; i++)
                dados.Add("");

            return dados;
        }
    }
}
