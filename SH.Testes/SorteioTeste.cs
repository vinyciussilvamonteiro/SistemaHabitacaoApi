﻿using SH.modelos;
using SH.negocios.Implementacoes;
using SH.negocios.Interfaces;
using SH.repositorios.Implementacoes;
using SH.utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SH.Testes
{
    public class SorteioTeste
    {
        private readonly ISorteioNegocios sorteioNegocios;

        public SorteioTeste()
        {
            sorteioNegocios = new SorteioNegocios(
                new ParticipantesNegocios(
                    new ParticipantesRepositorio()    
                )
            );
        }

        [Fact]
        public void VencedoresEsperadosIdoso()
        {
            //Arranje
            List<CategoriaComParticipantes> grupos = sorteioNegocios.RecuperarVencedores();
            int vencedoresNecessariosCount = 1;

            //Act
            List<Participante> vencedores = grupos.FirstOrDefault(grupo => grupo.Nome.ToLower() == Constants.IDOSO).Participantes;


            //Assert
            Assert.Equal(vencedores.Count, vencedoresNecessariosCount);
        }


        [Fact]
        public void VencedoresEsperadosDeficiente()
        {
            //Arranje
            List<CategoriaComParticipantes> grupos = sorteioNegocios.RecuperarVencedores();
            int vencedoresNecessariosCount = 1;

            //Act
            List<Participante> vencedores = grupos.FirstOrDefault(grupo => grupo.Nome.ToLower() == Constants.DEFICIENTE).Participantes;


            //Assert
            Assert.Equal(vencedores.Count, vencedoresNecessariosCount);
        }


        [Fact]
        public void VencedoresEsperadosGeral()
        {
            //Arranje
            List<CategoriaComParticipantes> grupos = sorteioNegocios.RecuperarVencedores();
            int vencedoresNecessariosCount = 3;

            //Act
            List<Participante> vencedores = grupos.FirstOrDefault(grupo => grupo.Nome.ToLower() == Constants.GERAL).Participantes;


            //Assert
            Assert.Equal(vencedores.Count, vencedoresNecessariosCount);
        }

        [Fact]
        public void VencedoresEsperadosTodos()
        {
            //Arranje
            List<CategoriaComParticipantes> grupos = sorteioNegocios.RecuperarVencedores();
            int vencedoresNecessariosCount = 5;

            //Act
            List<Participante> vencedores = new();
            grupos.ForEach(grupo => vencedores.AddRange(grupo.Participantes));


            //Assert
            Assert.Equal(vencedores.Count, vencedoresNecessariosCount);
        }


    }
}
