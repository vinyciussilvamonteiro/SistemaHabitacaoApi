﻿using SH.modelos;
using SH.negocios.Interfaces;
using SH.repositorios.Implementacoes;
using SH.repositorios.Interfaces;
using SH.utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SH.Testes
{
    public class ParticipantesTestes
    {
        private readonly IParticipantesRepositorio _participantesRepositorio;
        public ParticipantesTestes()
        {
            _participantesRepositorio = new ParticipantesRepositorio();
        }

        [Fact]
        public void TesteValidarGrupoIdososAptos()
        {
            //Arrange
            List<Participante> participantesIdoso = _participantesRepositorio.RecuperarIdososAptos();

            //Act
            bool cpfValidos = participantesIdoso.All(participante => DadosUtilidades.ChecarValidadeCpf(participante.Cpf));
            bool nomesValidos = participantesIdoso.All(participante => DadosUtilidades.ChecarNome(participante.Nome));
            bool datasValidas = participantesIdoso.All(participante => DadosUtilidades.ChecarValidadeDataNascimento(participante.DataNascimento.ToString()));
            bool rendaValido = participantesIdoso.All(participante => participante.Renda >= Constants.RENDACORTEMINIMO && participante.Renda <= Constants.RENDACORTEMAXIMO);
            bool idadeValida = participantesIdoso.All(participante => participante.Idade > Constants.IDADEIDOSO);
            bool grupoValido = participantesIdoso.All(participante => participante.Cota.ToLower() == Constants.IDOSO);
            bool cidValidado = participantesIdoso.All(participante => string.IsNullOrEmpty(participante.Cid));


            //Assert
            Assert.True(cpfValidos && nomesValidos && datasValidas && rendaValido && idadeValida && grupoValido);
        }

        [Fact]
        public void TesteValidarGrupoDeficientesAptos()
        {
            //Arrange
            List<Participante> participantesDeficientes = _participantesRepositorio.RecuperarDeficientesAptos();

            //Act
            bool cpfValidos = participantesDeficientes.All(participante => DadosUtilidades.ChecarValidadeCpf(participante.Cpf));
            bool nomesValidos = participantesDeficientes.All(participante => DadosUtilidades.ChecarNome(participante.Nome));
            bool datasValidas = participantesDeficientes.All(participante => DadosUtilidades.ChecarValidadeDataNascimento(participante.DataNascimento.ToString()));
            bool rendaValido = participantesDeficientes.All(participante => participante.Renda >= Constants.RENDACORTEMINIMO && participante.Renda <= Constants.RENDACORTEMAXIMO);
            bool idadeValida = participantesDeficientes.All(participante => participante.Idade > Constants.IDADEMINIMA);
            bool grupoValido = participantesDeficientes.All(participante => participante.Cota.ToLower() == Constants.DEFICIENTE);
            bool cidValidado = participantesDeficientes.All(participante => !string.IsNullOrEmpty(participante.Cid));


            //Assert
            Assert.True(cpfValidos && nomesValidos && datasValidas && rendaValido && idadeValida && grupoValido);
        }

        [Fact]
        public void TesteValidarGrupoGeral()
        {
            //Arrange
            List<Participante> participantesGeral = _participantesRepositorio.RecuperarGeralAptos();

            //Act
            bool cpfValidos = participantesGeral.All(participante => DadosUtilidades.ChecarValidadeCpf(participante.Cpf));
            bool nomesValidos = participantesGeral.All(participante => DadosUtilidades.ChecarNome(participante.Nome));
            bool datasValidas = participantesGeral.All(participante => DadosUtilidades.ChecarValidadeDataNascimento(participante.DataNascimento.ToString()));
            bool rendaValido = participantesGeral.All(participante => participante.Renda >= Constants.RENDACORTEMINIMO && participante.Renda <= Constants.RENDACORTEMAXIMO);
            bool idadeValida = participantesGeral.All(participante => participante.Idade > Constants.IDADEMINIMA);
            bool grupoValido = participantesGeral.All(participante => participante.Cota.ToLower() == Constants.GERAL);
            bool cidValidado = participantesGeral.All(participante => string.IsNullOrEmpty(participante.Cid));

            //Assert
            Assert.True(cpfValidos && nomesValidos && datasValidas && rendaValido && idadeValida && grupoValido);
        }

    }
}
