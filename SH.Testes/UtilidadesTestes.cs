﻿using SH.utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SH.Testes
{
    public class UtilidadesTestes
    {
        [Fact]
        public void TestarValidadorCpf()
        {
            //Arrange
            List<string> cpFValidos = new()
            {
                "863.671.240-80",
                "830.560.200-18",
                "579.377.950-77",
                "277.035.330-68",
                "772.738.980-31"
            };

            List<string> cpfInvalidos = new()
            {
                "863.671.240",
                "830.560.790-78",
                "579.3747.950-77",
                "277035330680",
                "772.738.980-00"
            };

            //Act
            bool todosValidos = cpFValidos.All(cpf => DadosUtilidades.ChecarValidadeCpf(cpf));
            bool nenhumValido = !cpfInvalidos.Any(cpf => DadosUtilidades.ChecarValidadeCpf(cpf));


            //Assert
            Assert.True(todosValidos && nenhumValido);
        }

        [Fact]
        public void TestarValidarNome()
        {
            //Arrange
            List<string> nomesValidos = new()
            {
                "Vinycius da Silva Monteiro",
                "Adailton Claudio Môura",
                "Ana Biterct silvã",
                "Cláudio arraujo santos",
                "Michele crisalda sants"
            };

            List<string> nomesInvalidos = new()
            {
                "M1lla Jartison",
                "123-de oliveira 4",
                "Raul jo~ao",
                "el-julgador",
                "mariati44"
            };

            //Act
            bool todosValidos = nomesValidos.All(nome => DadosUtilidades.ChecarNome(nome));
            bool nenhumValido = !nomesInvalidos.Any(nome => DadosUtilidades.ChecarNome(nome));


            //Assert
            Assert.True(todosValidos && nenhumValido);
        }

        [Fact]
        public void TestarValidarNumeros()
        {
            //Arrange
            List<string> numerosValidos = new()
            {
                "2014.50",
                "1205",
                "2122",
                "44,22",
                "9999999999999999999999999"
            };

            List<string> numerosInvalidos = new()
            {
                "2014-50",
                "2014 65",
                "f014.12",
                "f12",
                "2525f5"
            };

            //Act
            bool todosValidos = numerosValidos.All(numero => DadosUtilidades.ChecarValorNumerico(numero));
            bool nenhumValido = !numerosInvalidos.Any(numero => DadosUtilidades.ChecarValorNumerico(numero));


            //Assert
            Assert.True(todosValidos && nenhumValido);
        }

        [Fact]
        public void TestarGrupoValido()
        {
            //Arrange
        

            //Act
            bool valido =  DadosUtilidades.ChecarGruposValidos("teste", new List<string>() {"teste", "testar", "tester"});
            bool invalido = !DadosUtilidades.ChecarGruposValidos("teste", new List<string>() { "testador", "testar", "tester" });


            //Assert
            Assert.True(valido && invalido);
        }
    }
}
