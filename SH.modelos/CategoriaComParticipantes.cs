﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SH.modelos
{
    public class CategoriaComParticipantes
    {
        public string Nome {get; set;}
        public List<Participante> Participantes { get; set; }
    }
}
