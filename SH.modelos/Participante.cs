﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SH.modelos
{
    public class Participante
    {

        public Participante(){}
        public Participante(string nome, string cpf, string dataNascimento, string renda, string cota, string cid)
        {
            Nome = nome;
            Cpf = cpf;
            DataNascimento = DateTime.Parse(dataNascimento);
            Renda = decimal.Parse(renda.Replace(".", ","));
            Cota = cota;
            Cid = cid;
            Idade = (DateTime.Now - DateTime.Parse(dataNascimento)).Days / 365.25;
        }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public DateTime DataNascimento { get; set; }
        public decimal Renda { get; set; }
        public string Cota { get; set; }
        public string Cid { get; set; }
        public double Idade { get; set; }
    }
}
