﻿using SH.modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SH.negocios.Interfaces
{
    public interface IParticipantesNegocios
    {
        List<Participante> RecuperarIdososAptos();
        List<Participante> RecuperarDeficientesAptos();
        List<Participante> RecuperarGeralAptos();
        List<CategoriaComParticipantes> RecuperarTodosAptos();
    }
}
