﻿using SH.modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SH.negocios.Interfaces
{
    public interface ISorteioNegocios
    {
        List<CategoriaComParticipantes> RecuperarVencedores();
    }
}
