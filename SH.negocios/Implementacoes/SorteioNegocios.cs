﻿using SH.modelos;
using SH.negocios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SH.utilidades;
using System.Threading.Tasks;

namespace SH.negocios.Implementacoes
{
    
    public class SorteioNegocios : ISorteioNegocios
    {

        private readonly List<string> grupoUmSorteio = new() { Constants.IDOSO, Constants.DEFICIENTE };
        private readonly IParticipantesNegocios _participantesNegocios;
        public SorteioNegocios(IParticipantesNegocios participantesNegocios)
        {
            _participantesNegocios = participantesNegocios;
        }
        public List<CategoriaComParticipantes> RecuperarVencedores()
        {
            List<CategoriaComParticipantes> gruposComParticipantesAptos = _participantesNegocios.RecuperarTodosAptos();
            List<CategoriaComParticipantes> vencedores = new();
            gruposComParticipantesAptos.ForEach(grupo =>
            {
                    vencedores.Add(
                        new CategoriaComParticipantes()
                        {
                            Nome = grupo.Nome,
                            Participantes = RecuperarVencedoresGrupo(
                                grupo.Participantes,
                                grupoUmSorteio.Contains(grupo.Nome.ToLower()) ? 1 : 3
                            )
                        }

                    );

            });

            return vencedores;


        }


        private List<Participante> RecuperarVencedoresGrupo(List<Participante> participantes, int sorteios)
        {
            List<Participante> vencedores = new();
            Random rnd = new Random();
            for (int i = 0; i < sorteios; i++)
            {
                int sorteado = rnd.Next(0, participantes.Count - 1);
                Participante vencedor = participantes[sorteado];
                vencedores.Add(vencedor);
                participantes.RemoveAt(sorteado);

            }

            return vencedores;
                


        }
    }
}
