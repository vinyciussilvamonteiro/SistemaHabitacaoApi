﻿using SH.modelos;
using SH.negocios.Interfaces;
using SH.repositorios.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace SH.negocios.Implementacoes
{
    public class ParticipantesNegocios : IParticipantesNegocios
    {
        private readonly IParticipantesRepositorio _participantesRepositorio;
        public ParticipantesNegocios(IParticipantesRepositorio participantesRepositorio)
        {
            _participantesRepositorio = participantesRepositorio;
        }

        public List<Participante> RecuperarIdososAptos()
        {
            List<Participante> Idosos = _participantesRepositorio.RecuperarIdososAptos().Select(NormalizarParticipante)
            .ToList();
            return Idosos;
        }

        public List<Participante> RecuperarDeficientesAptos()
        {
            List<Participante> Deficientes = _participantesRepositorio.RecuperarDeficientesAptos().Select(NormalizarParticipante)
            .ToList();
            return Deficientes;
        }

        public List<Participante> RecuperarGeralAptos()
        {
            List<Participante> Geral = _participantesRepositorio.RecuperarGeralAptos().Select(NormalizarParticipante)
            .ToList();
            return Geral;
        }

        public List<CategoriaComParticipantes> RecuperarTodosAptos()
        {
            List<CategoriaComParticipantes> Todos = _participantesRepositorio
                .RecuperarTodosAptos()
                .ToList();

            Todos.ForEach(grupo =>
            {

                grupo.Participantes = grupo.Participantes.Select(NormalizarParticipante)
                .ToList();

            });
                
                
            return Todos;
        }

        private static Participante NormalizarParticipante(Participante participante) => new ()
        {
            Nome = participante.Nome,
            Cpf = participante.Cpf,
            Renda = participante.Renda,
            Idade = participante.Idade,
            Cid = participante.Cid,
            DataNascimento = participante.DataNascimento,
            Cota = participante.Cota
        };

    }
}
