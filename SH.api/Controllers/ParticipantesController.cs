﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SH.modelos;
using SH.negocios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SH.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParticipantesController : ControllerBase
    {
        private readonly IParticipantesNegocios _participantesNegocios;
        public ParticipantesController(IParticipantesNegocios participantesNegocios)
        {
            _participantesNegocios = participantesNegocios;
        }
        [HttpGet("aptos/idosos")]
        public ActionResult RecuperarIdososAptos()
        {
            try
            {
                List<Participante> Idosos = _participantesNegocios.RecuperarIdososAptos();
                return Ok(Idosos);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Algo de errado aconteceu ao processar a requisição");
            }
        }

        [HttpGet("aptos/deficientes")]
        public ActionResult RecuperarDeficienteAptos()
        {
            try
            {
                List<Participante> deficientes = _participantesNegocios.RecuperarDeficientesAptos();
                return Ok(deficientes);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Algo de errado aconteceu ao processar a requisição");
            }
        }
        [HttpGet("aptos/geral")]
        public ActionResult RecuperarGeralAptos()
        {
            try
            {
                List<Participante> geral = _participantesNegocios.RecuperarGeralAptos();
                return Ok(geral);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Algo de errado aconteceu ao processar a requisição");
            }
        }

        [HttpGet("aptos/todos")]
        public ActionResult RecuperarTodosAptos()
        {
            try
            {
                List<CategoriaComParticipantes> todos = _participantesNegocios.RecuperarTodosAptos();
                return Ok(todos);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Algo de errado aconteceu ao processar a requisição");
            }
        }

    }
}
