﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SH.modelos;
using SH.negocios.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SH.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SorteiosController : ControllerBase
    {
        private readonly ISorteioNegocios _sorteioNegocios;
        public SorteiosController(ISorteioNegocios sorteioNegocios)
        {
            _sorteioNegocios = sorteioNegocios;
        }

        [HttpGet("realizar")]
        public ActionResult RealizarSorteio()
        {
            try
            {
                List<CategoriaComParticipantes> vencedores = _sorteioNegocios.RecuperarVencedores();
                return Ok(vencedores);
            }
            catch (Exception e)
            {
                return StatusCode(500, "Algo de errado aconteceu ao processar a requisição");
            }
        }
    }
}
